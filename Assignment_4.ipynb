{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "_You are currently looking at **version 1.0** of this notebook. To download notebooks and datafiles, as well as get help on Jupyter notebooks in the Coursera platform, visit the [Jupyter Notebook FAQ](https://www.coursera.org/learn/python-machine-learning/resources/bANLa) course resource._\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Assignment 4 - Understanding and Predicting Property Maintenance Fines\n",
    "\n",
    "This assignment is based on a data challenge from the Michigan Data Science Team ([MDST](http://midas.umich.edu/mdst/)). \n",
    "\n",
    "The Michigan Data Science Team ([MDST](http://midas.umich.edu/mdst/)) and the Michigan Student Symposium for Interdisciplinary Statistical Sciences ([MSSISS](https://sites.lsa.umich.edu/mssiss/)) have partnered with the City of Detroit to help solve one of the most pressing problems facing Detroit - blight. [Blight violations](http://www.detroitmi.gov/How-Do-I/Report/Blight-Complaint-FAQs) are issued by the city to individuals who allow their properties to remain in a deteriorated condition. Every year, the city of Detroit issues millions of dollars in fines to residents and every year, many of these fines remain unpaid. Enforcing unpaid blight fines is a costly and tedious process, so the city wants to know: how can we increase blight ticket compliance?\n",
    "\n",
    "The first step in answering this question is understanding when and why a resident might fail to comply with a blight ticket. This is where predictive modeling comes in. For this assignment, your task is to predict whether a given blight ticket will be paid on time.\n",
    "\n",
    "All data for this assignment has been provided to us through the [Detroit Open Data Portal](https://data.detroitmi.gov/). **Only the data already included in your Coursera directory can be used for training the model for this assignment.** Nonetheless, we encourage you to look into data from other Detroit datasets to help inform feature creation and model selection. We recommend taking a look at the following related datasets:\n",
    "\n",
    "* [Building Permits](https://data.detroitmi.gov/Property-Parcels/Building-Permits/xw2a-a7tf)\n",
    "* [Trades Permits](https://data.detroitmi.gov/Property-Parcels/Trades-Permits/635b-dsgv)\n",
    "* [Improve Detroit: Submitted Issues](https://data.detroitmi.gov/Government/Improve-Detroit-Submitted-Issues/fwz3-w3yn)\n",
    "* [DPD: Citizen Complaints](https://data.detroitmi.gov/Public-Safety/DPD-Citizen-Complaints-2016/kahe-efs3)\n",
    "* [Parcel Map](https://data.detroitmi.gov/Property-Parcels/Parcel-Map/fxkw-udwf)\n",
    "\n",
    "___\n",
    "\n",
    "We provide you with two data files for use in training and validating your models: train.csv and test.csv. Each row in these two files corresponds to a single blight ticket, and includes information about when, why, and to whom each ticket was issued. The target variable is compliance, which is True if the ticket was paid early, on time, or within one month of the hearing data, False if the ticket was paid after the hearing date or not at all, and Null if the violator was found not responsible. Compliance, as well as a handful of other variables that will not be available at test-time, are only included in train.csv.\n",
    "\n",
    "Note: All tickets where the violators were found not responsible are not considered during evaluation. They are included in the training set as an additional source of data for visualization, and to enable unsupervised and semi-supervised approaches. However, they are not included in the test set.\n",
    "\n",
    "<br>\n",
    "\n",
    "**File descriptions** (Use only this data for training your model!)\n",
    "\n",
    "    train.csv - the training set (all tickets issued 2004-2011)\n",
    "    test.csv - the test set (all tickets issued 2012-2016)\n",
    "    addresses.csv & latlons.csv - mapping from ticket id to addresses, and from addresses to lat/lon coordinates. \n",
    "     Note: misspelled addresses may be incorrectly geolocated.\n",
    "\n",
    "<br>\n",
    "\n",
    "**Data fields**\n",
    "\n",
    "train.csv & test.csv\n",
    "\n",
    "    ticket_id - unique identifier for tickets\n",
    "    agency_name - Agency that issued the ticket\n",
    "    inspector_name - Name of inspector that issued the ticket\n",
    "    violator_name - Name of the person/organization that the ticket was issued to\n",
    "    violation_street_number, violation_street_name, violation_zip_code - Address where the violation occurred\n",
    "    mailing_address_str_number, mailing_address_str_name, city, state, zip_code, non_us_str_code, country - Mailing address of the violator\n",
    "    ticket_issued_date - Date and time the ticket was issued\n",
    "    hearing_date - Date and time the violator's hearing was scheduled\n",
    "    violation_code, violation_description - Type of violation\n",
    "    disposition - Judgment and judgement type\n",
    "    fine_amount - Violation fine amount, excluding fees\n",
    "    admin_fee - $20 fee assigned to responsible judgments\n",
    "state_fee - $10 fee assigned to responsible judgments\n",
    "    late_fee - 10% fee assigned to responsible judgments\n",
    "    discount_amount - discount applied, if any\n",
    "    clean_up_cost - DPW clean-up or graffiti removal cost\n",
    "    judgment_amount - Sum of all fines and fees\n",
    "    grafitti_status - Flag for graffiti violations\n",
    "    \n",
    "train.csv only\n",
    "\n",
    "    payment_amount - Amount paid, if any\n",
    "    payment_date - Date payment was made, if it was received\n",
    "    payment_status - Current payment status as of Feb 1 2017\n",
    "    balance_due - Fines and fees still owed\n",
    "    collection_status - Flag for payments in collections\n",
    "    compliance [target variable for prediction] \n",
    "     Null = Not responsible\n",
    "     0 = Responsible, non-compliant\n",
    "     1 = Responsible, compliant\n",
    "    compliance_detail - More information on why each ticket was marked compliant or non-compliant\n",
    "\n",
    "\n",
    "___\n",
    "\n",
    "## Evaluation\n",
    "\n",
    "Your predictions will be given as the probability that the corresponding blight ticket will be paid on time.\n",
    "\n",
    "The evaluation metric for this assignment is the Area Under the ROC Curve (AUC). \n",
    "\n",
    "Your grade will be based on the AUC score computed for your classifier. A model which with an AUROC of 0.7 passes this assignment, over 0.75 will recieve full points.\n",
    "___\n",
    "\n",
    "For this assignment, create a function that trains a model to predict blight ticket compliance in Detroit using `train.csv`. Using this model, return a series of length 61001 with the data being the probability that each corresponding ticket from `test.csv` will be paid, and the index being the ticket_id.\n",
    "\n",
    "Example:\n",
    "\n",
    "    ticket_id\n",
    "       284932    0.531842\n",
    "       285362    0.401958\n",
    "       285361    0.105928\n",
    "       285338    0.018572\n",
    "                 ...\n",
    "       376499    0.208567\n",
    "       376500    0.818759\n",
    "       369851    0.018528\n",
    "       Name: compliance, dtype: float32"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 190,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import numpy as np\n",
    "\n",
    "def read_csv_eri(file_path, **kwargs):\n",
    "    with open(file_path, 'r', errors='ignore') as csv:\n",
    "        return pd.read_csv(csv, quotechar='\"', **kwargs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 218,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'agency_name',\n",
       " 'balance_due',\n",
       " 'collection_status',\n",
       " 'compliance',\n",
       " 'compliance_detail',\n",
       " 'discount_amount',\n",
       " 'disposition',\n",
       " 'fine_amount',\n",
       " 'hearing_date',\n",
       " 'inspector_name',\n",
       " 'judgment_amount',\n",
       " 'late_fee',\n",
       " 'payment_amount',\n",
       " 'payment_date',\n",
       " 'payment_status',\n",
       " 'ticket_id',\n",
       " 'ticket_issued_date',\n",
       " 'violation_code',\n",
       " 'violator_name',\n",
       " 'zip_code'}"
      ]
     },
     "execution_count": 218,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "train_cols = pd.read_csv('train.csv', nrows=1, encoding = 'ISO-8859-1')\n",
    "set(train_cols.columns.values) - set(['violation_description', 'admin_fee', 'state_fee', 'clean_up_cost', 'grafitti_status',\n",
    "                     'violation_street_number', 'violation_street_name', 'violation_zip_code', \n",
    "                     'mailing_address_str_number', 'mailing_address_str_name', \n",
    "                     'city', 'state', 'country', 'non_us_str_code'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 220,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "/opt/conda/lib/python3.6/site-packages/IPython/core/interactiveshell.py:2728: DtypeWarning: Columns (11) have mixed types. Specify dtype option on import or set low_memory=False.\n",
      "  interactivity=interactivity, compiler=compiler, result=result)\n"
     ]
    }
   ],
   "source": [
    "excluded = {'violation_description', 'admin_fee', 'state_fee', 'clean_up_cost', 'grafitti_status',\n",
    "            'violation_street_number', 'violation_street_name', 'violation_zip_code', \n",
    "            'mailing_address_str_number', 'mailing_address_str_name', \n",
    "            'city', 'state', 'country', 'non_us_str_code'}\n",
    "\n",
    "def filter_cols(c):\n",
    "    return c not in excluded\n",
    "\n",
    "def calculate_rows(file):\n",
    "    df = pd.read_csv(file, nrows=1, encoding = 'ISO-8859-1')\n",
    "    return set(df.columns.values) - excluded\n",
    "    \n",
    "\n",
    "# train = read_csv_eri('train.csv', usecols=filter_cols)\n",
    "# test = read_csv_eri('test.csv', usecols=filter_cols)\n",
    "# addrs = read_csv_eri('addresses.csv')\n",
    "# latlons = read_csv_eri('latlons.csv')\n",
    "\n",
    "train = pd.read_csv('train.csv', usecols=calculate_rows('train.csv'), encoding = 'ISO-8859-1', quotechar='\"')\n",
    "test = pd.read_csv('test.csv', usecols=calculate_rows('test.csv'), encoding = 'ISO-8859-1', quotechar='\"')\n",
    "addrs = pd.read_csv('addresses.csv', encoding = 'ISO-8859-1', quotechar='\"')\n",
    "latlons = pd.read_csv('latlons.csv', encoding = 'ISO-8859-1', quotechar='\"')\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "indices_test = test['ticket_id'].values\n",
    "indices_train = train['ticket_id'].values\n",
    "\n",
    "for df in [train, test, addrs]:\n",
    "    df.set_index('ticket_id', inplace=True)\n",
    "\n",
    "\n",
    "assert(len(np.intersect1d(indices_test, indices_train)) == 0), \"train and test indices must not intersect\"\n",
    "assert(len(np.intersect1d(indices_test, train.index.values)) == 0) , \"train and test indices must not intersect\"\n",
    "assert(len(np.intersect1d(indices_train, test.index.values)) == 0) , \"train and test indices must not intersect\"\n",
    "\n",
    "# len(train), len(test)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 192,
   "metadata": {},
   "outputs": [],
   "source": [
    "# cleanup train\n",
    "## transforming to Y binary labels\n",
    "### remove non responsible persons from train\n",
    "train.dropna(subset=['compliance'], inplace=True)\n",
    "train['compliance'].value_counts()\n",
    "indices_train_after = train.index.values\n",
    "\n",
    "### remove unnecessary features\n",
    "to_drop_ = ['payment_amount', 'payment_date', 'payment_status', \n",
    "            'balance_due', 'collection_status', 'compliance_detail']\n",
    "train.drop(to_drop_, axis=1, inplace=True)\n",
    "\n",
    "# len(indices_test) + len(indices_train_after) , len(train) + len(test)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 193,
   "metadata": {},
   "outputs": [],
   "source": [
    "# concat train and test for preprocessing\n",
    "\n",
    "test['compliance'] = None\n",
    "test['dist_type'] = 'test'\n",
    "train['dist_type'] = 'train'\n",
    "\n",
    "raw_ds = pd.concat([train, test])\n",
    "\n",
    "# cleanup strings from zip-codes\n",
    "raw_ds['zip_code'] = pd.to_numeric(test['zip_code'], errors='coerce').fillna(0)\n",
    "# len(raw_ds), len(indices_test) + len(indices_train_after)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 194,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "# attach lat and lon, drop address\n",
    "raw_ds = raw_ds.reset_index().merge(addrs, how='left', left_on='ticket_id', right_index=True)\n",
    "raw_ds = raw_ds.merge(latlons, on='address', how='left')\n",
    "raw_ds.set_index('ticket_id', inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 195,
   "metadata": {},
   "outputs": [],
   "source": [
    "raw_ds.drop('address', axis=1, inplace=True)\n",
    "\n",
    "# sort by zip code and fillna on lat lon (nearest)\n",
    "raw_ds.sort_values(by='zip_code', inplace=True)\n",
    "raw_ds['lat'].fillna(axis=0, method='ffill', inplace=True)\n",
    "raw_ds['lon'].fillna(axis=0, method='ffill', inplace=True)\n",
    "\n",
    "\n",
    "# create interval between dates as a feature\n",
    "raw_ds['ticket_issued_date'] = pd.to_datetime(raw_ds['ticket_issued_date'])\n",
    "raw_ds['hearing_date'] = pd.to_datetime(raw_ds['hearing_date'])\n",
    "raw_ds['hear_period'] = abs(raw_ds['ticket_issued_date'] - raw_ds['hearing_date']).dt.days.fillna(0)\n",
    "raw_ds.drop(['ticket_issued_date', 'hearing_date'], axis=1, inplace=True)\n",
    "\n",
    "from sklearn import preprocessing as prp\n",
    "# to labels\n",
    "l_cols_ = ['violation_code', 'disposition', 'zip_code']\n",
    "l_cols_ += ['agency_name', 'inspector_name', 'violator_name']\n",
    "l_encs = dict()\n",
    "for atr in l_cols_:\n",
    "    l_encs[atr] = prp.LabelEncoder().fit(raw_ds[atr].astype(str))\n",
    "    raw_ds[atr] = l_encs[atr].transform(raw_ds[atr].astype(str))\n",
    "    \n",
    "\n",
    "# recreate train and test sets\n",
    "train = raw_ds[raw_ds['dist_type'] == 'train'].copy()\n",
    "train.drop('dist_type', axis=1, inplace=True)\n",
    "test = raw_ds[raw_ds['dist_type'] == 'test'].copy()\n",
    "test.drop('dist_type', axis=1, inplace=True)\n",
    "del raw_ds\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 196,
   "metadata": {},
   "outputs": [],
   "source": [
    "# normalisation\n",
    "test.drop('compliance', axis=1, inplace=True)\n",
    "# test.reset_index(inplace=True)\n",
    "# test.rename({'index': 'ticket_id'}, axis=1, inplace=True)\n",
    "\n",
    "X = train.iloc[:, train.columns != 'compliance']\n",
    "y = train['compliance'].astype('int8').values\n",
    "del train\n",
    "\n",
    "from sklearn import preprocessing as prp\n",
    "scaler = prp.MinMaxScaler()\n",
    "X = scaler.fit_transform(X)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 197,
   "metadata": {},
   "outputs": [],
   "source": [
    "# sanity check\n",
    "assert((61001 - len(test)) == 0), \"Test set became invalid after preprocessing\"\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 198,
   "metadata": {},
   "outputs": [],
   "source": [
    "# train test split\n",
    "from sklearn.model_selection import train_test_split\n",
    "\n",
    "X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.15, random_state=0)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 199,
   "metadata": {},
   "outputs": [],
   "source": [
    "# go for fun!\n",
    "\n",
    "GLOBAL_SCORES = dict()\n",
    "\n",
    "from sklearn.metrics import auc\n",
    "from sklearn.model_selection import cross_validate\n",
    "\n",
    "def cv_(estimator):\n",
    "    \"\"\"\n",
    "    Accesses global variables X_train, y_train, sklearn.model_selection.cross_validate\n",
    "    Returns tuple of 2 parts:\n",
    "    1) tuple of means of train and test score\n",
    "    2) scores dictionary\n",
    "    \"\"\"\n",
    "    scores = cross_validate(estimator, X_train, y_train, \n",
    "                          scoring='roc_auc', n_jobs=3, return_train_score=True)\n",
    "    return (np.mean(scores['train_score']), np.mean(scores['test_score'])), scores"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 200,
   "metadata": {},
   "outputs": [],
   "source": [
    "# GAUSSIAN naive Bayes baseline\n",
    "from sklearn.naive_bayes import GaussianNB\n",
    "est = GaussianNB()\n",
    "GLOBAL_SCORES['gaussian_nb'], scores = cv_(est)\n",
    "# scores"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 201,
   "metadata": {},
   "outputs": [],
   "source": [
    "# from sklearn.ensemble import RandomForestClassifier\n",
    "# est = RandomForestClassifier(n_estimators=20, max_depth=3)\n",
    "# GLOBAL_SCORES['random_forests'], scores = cv_(est)\n",
    "# scores"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 202,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.ensemble import GradientBoostingClassifier\n",
    "gbdt = GradientBoostingClassifier()\n",
    "GLOBAL_SCORES['gbdt'], scores = cv_(gbdt)\n",
    "# scores"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 203,
   "metadata": {},
   "outputs": [],
   "source": [
    "# from sklearn.svm import SVC\n",
    "# est = SVC()\n",
    "# GLOBAL_SCORES['svm'], scores = cv_(est)\n",
    "# scores"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 204,
   "metadata": {},
   "outputs": [],
   "source": [
    "# from sklearn.neural_network import mport ort MLPClassifier\n",
    "# est = MLPClassifier(hidden_layer_size%%bash[3,32], batch_size=64)\n",
    "# GLOBAL_SCORES['nn'], scores = cv_(est)\n",
    "# scores"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 205,
   "metadata": {},
   "outputs": [],
   "source": [
    "# # grid search on Gradient boosting\n",
    "# from sklearn.grid_search import GridSearchCV\n",
    "# from sklearn.ensemble import GradientBoostingClassifier\n",
    "\n",
    "# est = GradientBoostingClassifier()\n",
    "# grid_params = {'learning_rate': [0.1, 0.3, 0.5, 1.3, 2.1], \n",
    "#                'n_estimators':[50, 100, 150], \n",
    "#                'max_depth':[2,3,4], \n",
    "#                'max_features':[5,10,None]}\n",
    "# gs = GridSearchCV(est, grid_params, cv=3, scoring='roc_auc', n_jobs=4)\n",
    "# gs.fit(X_train, y_train)\n",
    "\n",
    "# print('Grid best parameter (max. AUC): ', gs.best_params_)\n",
    "# print('Grid best score (AUC): ', gs.best_score_)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 206,
   "metadata": {},
   "outputs": [],
   "source": [
    "# test['predictions'] = \n",
    "X_final_test = scaler.transform(test.iloc[:, test.columns != 'ticket_id'].values)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 207,
   "metadata": {},
   "outputs": [],
   "source": [
    "gbdt.fit(X_train, y_train)\n",
    "preds = gbdt.predict_proba(X_final_test)\n",
    "# preds[:,1]\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 208,
   "metadata": {},
   "outputs": [],
   "source": [
    "# from matplotlib import pyplot as plt\n",
    "\n",
    "# plt.figure()\n",
    "# plt.title(\"Feature importances\")\n",
    "# plt.bar(range(X_train.shape[1]), gbdt.feature_importances_,\n",
    "#        color=\"r\", align=\"center\")\n",
    "# plt.xlim([-1, X_train.shape[1]])\n",
    "# plt.show()\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 209,
   "metadata": {},
   "outputs": [],
   "source": [
    "test['predictions'] = preds[:,1]\n",
    "test['predictions'].to_csv('final_preds.csv')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 210,
   "metadata": {},
   "outputs": [],
   "source": [
    "# tst = pd.read_csv('final_preds.csv', names=['ticket_id', 'predict'], index_col='ticket_id')\n",
    "# tst['predict'].head()\n",
    "\n",
    "def blight_model():\n",
    "    return test['predictions']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 211,
   "metadata": {},
   "outputs": [],
   "source": [
    "# bm = blight_model()\n",
    "# bm.index.name = 'ticket_id'\n",
    "# res = 'Data type Test: '\n",
    "# res += ['Failed: type(bm) should Series\\n','Passed\\n'][type(bm)==pd.Series]\n",
    "# res += 'Data shape Test: '\n",
    "# res += ['Failed: len(bm) should be 61001\\n','Passed\\n'][len(bm)==61001]\n",
    "# res += 'Data Values Test: '\n",
    "# res += ['Failed: all values should be in [0.,1.]\\n','Passed\\n'][all((bm<=1.) & (bm>=0.))]\n",
    "# res += 'Data Values type Test: '\n",
    "# res += ['Failed: bm.dtype should be float\\n','Passed\\n'][str(bm.dtype).count('float')>0]\n",
    "# res += 'Index type Test: '\n",
    "# res += ['Failed: type(bm.index) should be Int64Index\\n','Passed\\n'][type(bm.index)==pd.Int64Index]\n",
    "# res += 'Index values type Test: '\n",
    "# res += ['Failed: type(bm.index[0]) should be int64\\n','Passed\\n'][str(type(bm.index[0])).count(\"int64\")>0]\n",
    "\n",
    "# res += 'Output index shape test:'\n",
    "# res += ['Failed, bm.index.shape should be (61001,)\\n','Passed\\n'][bm.index.shape==(61001,)]\n",
    "\n",
    "# res += 'Output index test: '\n",
    "# if bm.index.shape==(61001,):\n",
    "#     res +=['Failed\\n','Passed\\n'][all(pd.read_csv('test.csv',usecols=[0],index_col=0).sort_index().index.values==bm.sort_index().index.values)]\n",
    "# else:\n",
    "#     res+='Failed'\n",
    "# print(res)\n",
    "\n",
    "# pd.read_csv('test.csv',usecols=[0],index_col=0).sort_index().index.values[:5], bm.sort_index().index.values[:5]"
   ]
  }
 ],
 "metadata": {
  "coursera": {
   "course_slug": "python-machine-learning",
   "graded_item_id": "nNS8l",
   "launcher_item_id": "yWWk7",
   "part_id": "w8BSS"
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
